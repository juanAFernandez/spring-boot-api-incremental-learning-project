# Spring Boot API Incremental Learning Project

An incremental way to learn how to work with Spring Boot to build APIs.

## Current branches status

The first name is actually the content of the main branch.

### [basicCRUDWithTestingNoDB]

The next step, with an endpoint to make all CRUD operations using a very basic Message data object. There are not 
persistence in database but a repository is used to emulate one. Also, all tests are provided.

```
- controllers
  - MessageController.kt
- models
  - Message
- repositories
  - Message.kt  
```


### [helloWorld](https://gitlab.com/juanAFernandez/spring-boot-api-incremental-learning-project/-/tree/helloWorld)

A very basic hello world to check that the execution of Spring Boot is running 
correctly.

## Doc

The general documentation and details is available [here](doc/README.md).
## How to run?

    ./gradlew bR

The API will be exposed by default on port 8080.
If you enter to http://localhost:8080/ you must see 'Great job!' message. 

### How to execute tests?

To run all tests
`./gradlew test`

To run just a file
`./gradlew test --tests MessageControllerTest`

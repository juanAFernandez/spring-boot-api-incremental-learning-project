package com.springbootapirest.app.repositories

import com.springbootapirest.app.models.Message
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.web.server.ResponseStatusException

@Repository
class MessageRepository {

    var allMessages = arrayListOf<Message>()

    init {
        /*val s1 = Message(1, "Message title", "Message description");
        allMessages.add(s1)*/
    }

    fun getMessage(messageId: Long): Message? {
        return this.allMessages.find { message ->  message.id === messageId };
    }

    fun getAllMessages(): List<Message>{
        return allMessages;
    }

    fun addMessage(message: Message): Message {
        message.id = System.currentTimeMillis();
        this.allMessages.add(message);
        return message;
    }

    fun updateMessage(messageId: Long, title: String): Message {
        val message: Message? = this.getMessage(messageId)
        if (message !== null) {
            message.title = title
            return message;
        } else {
            throw ResponseStatusException(
                HttpStatus.NOT_FOUND, "entity not found"
            )
        }
    }

    fun deleteMessage(messageId: Long): ResponseEntity<String> {
        return ResponseEntity.status(HttpStatus.CREATED).body("Message deleted");
    }
}
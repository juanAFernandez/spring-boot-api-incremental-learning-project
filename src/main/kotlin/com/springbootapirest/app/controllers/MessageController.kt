package com.springbootapirest.app.controllers

import com.springbootapirest.app.models.Message
import com.springbootapirest.app.repositories.MessageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
class MessageResource {

    @Autowired
    val messagesRepo = MessageRepository()

    @GetMapping
    fun getAllMessages(): List<Message> {
        return messagesRepo.getAllMessages();
    }

    @PostMapping
    fun postMessage(@RequestBody message: Message): Message {
        println(message);
        return messagesRepo.addMessage(message);
    }

    @PutMapping("/{messageId}")
    fun putMessage(@PathVariable messageId: Long, @RequestBody message: Message): Message {
        return messagesRepo.updateMessage(messageId , message.title);
    }

    @GetMapping("/{messageId}")
    fun getMessage(@PathVariable messageId: Long): Message {
        val message: Message? = messagesRepo.getMessage(messageId);
        if(message !== null) {
            return message;
        } else {
            throw ResponseStatusException(
                HttpStatus.NOT_FOUND, "Message not found"
            )
        }

    }

    @DeleteMapping("/{messageId}")
    fun deleteMessage(@PathVariable messageId: Long): ResponseEntity<String> {
        return ResponseEntity.status(HttpStatus.CREATED).body("Message deleted");
    }

}
package com.springbootapirest.app.models


data class Message(var id: Long? = null , var title: String, var description: String = "")
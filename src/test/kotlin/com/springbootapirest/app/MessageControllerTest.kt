package com.springbootapirest.app

import com.springbootapirest.app.models.Message
import com.springbootapirest.app.repositories.MessageRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class MessageControllerTest {

    @Autowired
    val messagesRepo = MessageRepository();

    @Test
    fun addMessageAndRetrieveById() {
        val message = Message(title = "Message title", description = "Message description");
        val messageSaved = messagesRepo.addMessage(message);
        val messagesList = messagesRepo.getAllMessages();

        Assertions.assertEquals(messageSaved.title, messageSaved.title)
        Assertions.assertEquals(messageSaved.description, messageSaved.description)
        Assertions.assertNotEquals(messageSaved.id, null);
        Assertions.assertEquals(messagesList[0], messageSaved);

        val messageID = if(message.id === null) null else message.id;
        if (messageID !== null) {
            Assertions.assertEquals(messageID, messageSaved.id)
        }
    }

    @Test
    fun getMessageById() {
        // Fail if messageId doesn't exists
        var message = messagesRepo.getMessage(1); // Id 1 doesn't exists.
        Assertions.assertEquals(message, null);
        // Success if it exists.
        message = messagesRepo.addMessage(Message(title = "Message title", description = "Message description"));
        val messageID = if(message.id === null) null else message.id;
        /*if (messageID !== null) {
            Assertions.assertEquals(message.id, messagesRepo.getMessage(messageID)?.id)
        }*/
    }

}